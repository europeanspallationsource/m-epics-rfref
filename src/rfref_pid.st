program rfref_pid

%%#include <math.h>

/* Inputs sensors */
double s1;
assign s1 to "{PREFIX}:PID-SENS1";
double s2;
assign s2 to "{PREFIX}:PID-SENS2";
double yref;
assign yref to "{PREFIX}:PID-YREF";
monitor yref;

/* Inputs Constants */
double Ts = 0.5;
assign Ts to "{PREFIX}:PID-TS";
monitor Ts;
double K;
assign K to "{PREFIX}:PID-K";
monitor K;
double Ti;
assign Ti to "{PREFIX}:PID-TI";
monitor Ti;
double Td;
assign Td to "{PREFIX}:PID-TD";
monitor Td;
double N;
assign N to "{PREFIX}:PID-N";
monitor N;
double beta;
assign beta to "{PREFIX}:PID-BETA";
monitor beta;
double umin;
assign umin to "{PREFIX}:PID-UMIN";
monitor umin;
double umax;
assign umax to "{PREFIX}:PID-UMAX";
monitor umax;

/* Outputs */
double u;
assign u to "{PREFIX}:PID-CONTROL";
double e;
assign e to "{PREFIX}:PID-ERROR";
double y;
assign y to "{PREFIX}:PID-Y";

/* Intermediates */
double Tf;
double Tr;
double v;
double x1, x2, x1old, x2old;
double p1, p2, p3, p4, p5;
double den;
double I;
double TsOld;
double yrefOld;

ss pid {
	state init {
		when() {
			Tf = Ti/N;
			Tr = Ti/30;
			TsOld = Ts;
			yrefOld = yref;
			den = pow(Tf, 2) + 1.4*Ts*Tf + pow(Ts, 2);
			p1 = 1 - pow(Ts, 2)/den;
			p2 = Ts*pow(Tf, 2)/den;
			p3 = pow(Ts, 2)/den;
			p4 = p2/Ts;
			p5 = p3/Ts;
			y = (s1 + s2)/2;
			x1old = y;
			x2old = 0;
			I = 0;
			printf("Reset x1:%f, x2:%f, p %f, %f, %f, %f, %f\n", x1old, x2old, p1, p2, p3, p4, p5);
		} state running
	}
	state running {
		when(TsOld != Ts || yrefOld != yref) { } state init
		when(delay(Ts)) {
			pvGet(s1);
			pvGet(s2);
			y = (s1 + s2)/2;
			e = y - yref;
			pvPut(e);
			x1 = p1*x1old + p2*x2old + p3*y;
			x2 = p4*x2old + p5*(y-x1old);
			v = K*(beta*yref - x1) + I - K*Td*x2;
			u = fmax(fmin(v,umax),umin);
			pvPut(u);
			pvPut(y);
			//printf("iteration x1 %f x2 %f u %f v %f\n", x1, x2, u, v);
			I = I + (K*Ts/Ti)*(yref - x1) + 1/Tr*(u - v);
			x1old = x1;
			x2old = x2;
		} state running
	}
}
