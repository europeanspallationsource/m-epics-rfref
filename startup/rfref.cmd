require rfref,0.3.1

epicsEnvSet("PREFIX", "RFREF")

# Initialize ecat2 driver
# ecat2configure(domain, frequency [hz], autoconfig, autostart)
ecat2configure(0, 500, 1, 1)

# Load ecat2 records
dbLoadRecords("ecat2master.template", "PREFIX=$(PREFIX)")
dbLoadRecords("ecat2el2502.db", "PREFIX=${PREFIX}, MOD_ID=PWM0   , SLAVE_IDX=1")
dbLoadRecords("ecat2el3214.db", "PREFIX=${PREFIX}, MOD_ID=RTDMOD0, SLAVE_IDX=2")
dbLoadRecords("ecat2el3214.db", "PREFIX=${PREFIX}, MOD_ID=RTDMOD1, SLAVE_IDX=3")
dbLoadRecords("ecat2el3214.db", "PREFIX=${PREFIX}, MOD_ID=RTDMOD2, SLAVE_IDX=4")
dbLoadRecords("ecat2el4102.db", "PREFIX=${PREFIX}, MOD_ID=AOMOD0 , SLAVE_IDX=5")
dbLoadRecords("ecat2el4122.db", "PREFIX=${PREFIX}, MOD_ID=AOMOD1 , SLAVE_IDX=6")
dbLoadRecords("ecat2el3202.db", "PREFIX=${PREFIX}, MOD_ID=RTDHP0 , SLAVE_IDX=7")
dbLoadRecords("ecat2el3202.db", "PREFIX=${PREFIX}, MOD_ID=RTDHP1 , SLAVE_IDX=8")
dbLoadRecords("ecat2el3202.db", "PREFIX=${PREFIX}, MOD_ID=RTDHP2 , SLAVE_IDX=9")
dbLoadRecords("ecat2el3202.db", "PREFIX=${PREFIX}, MOD_ID=RTDHP3 , SLAVE_IDX=10")
dbLoadRecords("ecat2el3202.db", "PREFIX=${PREFIX}, MOD_ID=RTDHP4 , SLAVE_IDX=11")

# PID Controller macros
epicsEnvSet(CONTROL, "${PREFIX}:PWM0-DO1")
epicsEnvSet(SENS1,   "${PREFIX}:RTDHP3-RTD1")
epicsEnvSet(SENS2,   "${PREFIX}:RTDHP2-RTD2")

# PID Controller records
dbLoadRecords("rfref_pid.template", "PREFIX=${PREFIX}, YREF=30, TS=0.5, PB=0.59, TI=139, TD=0, N=50, BETA=1, UMIN=0, UMAX=100, SENS1=${SENS1}, SENS2=${SENS2}, CONTROL=${CONTROL}")

# PID Controller implementation
seq(rfref_pid, "PREFIX=${PREFIX}")
